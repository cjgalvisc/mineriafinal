# librerias Flask
import os
from flask import Flask, render_template, request, redirect, url_for
from werkzeug.utils import secure_filename
from flask.json import jsonify
import json
# Librerias DataMining
import numpy as np
from sklearn import cross_validation,model_selection, neighbors,preprocessing
from sklearn.naive_bayes import GaussianNB
import pandas as pd

app = Flask(__name__)
#variable global para guardar el nombre del archivo
nombreArchivo=''
#ruta inicial
@app.route('/')
def index():
   return render_template('index.html')

#ruta para escoger el algoritmo
@app.route('/direccionar',methods = ['GET','POST'])
def direccionar():
    if request.method == "POST":
        #guardamos el archivo
        f = request.files['archivo']
        global nombreArchivo
        nombreArchivo=f.filename
        f.save(secure_filename(nombreArchivo))
        # redireccionamos al algoritmo
        algoritmo = request.form.get('algoritmo')
        if(algoritmo=='1'):
            return render_template('redNeuronal/index.html')
        elif(algoritmo=='2'):
            return render_template('knn/index.html')
        elif(algoritmo=='3'):
            return render_template('gauss/index.html')

#ruta para el algoritmo KNN
@app.route('/knn',methods = ['GET','POST'])
def knn():
    if request.method == 'POST':
       global nombreArchivo
       ruta=nombreArchivo
       porcentajePrueba=float(request.form['porcentaje'])
       k=int(request.form['k'])
       datoTmp=request.form['caso']
       datoPrueba=(list(map(float,datoTmp.strip().split(','))))
       # leemos el archivo y reemplazamos los caracteres extraños
       df = pd.read_csv(ruta)
       df.replace('?',-99999, inplace=True)
       #extraemos los datos y los encabezados
       X = np.array(df.drop(['class'], 1))
       y = np.array(df['class'])
       #generamos los sets de entrenamiento y de prueba
       X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=porcentajePrueba)
       #creamos el modelo
       clf = neighbors.KNeighborsClassifier(n_neighbors=k)
       #entrenamos el modelo
       clf.fit(X_train, y_train)
       #hallamos la exactitud del modelo
       accuracy = clf.score(X_test, y_test)
       #ingresamos el dato de prueba
       example_measures = np.array([datoPrueba])
       example_measures = example_measures.reshape(len(example_measures), -1)
       #generamos la prediccion
       prediction = clf.predict(example_measures)
       #encontramos los vecinos mas cercanos
       distancias, indices=clf.kneighbors(example_measures, return_distance=True)
       #hallamos los valores de los vecinos
       vecinos=list()
       for indice in indices[0]:
           vecinos.append(df.loc[indice-1])
       n3=len(vecinos[0])
       #
       n=len(distancias[0])
       cabecera=df.columns
       n2=len(cabecera)
       clases=set(np.array(df['class']))
       return render_template('knn/result.html',n3=n3,clases=clases,n2=n2,cabecera=cabecera,vecinos=vecinos,ruta=ruta,k=k,datoPrueba=datoPrueba,porcentajePrueba=porcentajePrueba,distancias=distancias,indices=indices,n=n,accuracy=accuracy,prediction=prediction)

@app.route('/gauss',methods = ['GET','POST'])
def gauss():
    porcentajePrueba=float(request.form['porcentaje'])
    global nombreArchivo
    ruta=nombreArchivo
    datoTmp=request.form['caso']
    datoPrueba=(list(map(float,datoTmp.strip().split(','))))
    # leemos el archivo y reemplazamos los caracteres extraños
    df = pd.read_csv(ruta)
    df.replace('?',-99999, inplace=True)

    #extraemos los datos y los encabezados
    X = np.array(df.drop(['class'], 1))
    y = np.array(df['class'])

    #generamos los sets de entrenamiento y de prueba
    X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=porcentajePrueba)

    #example_measures
    example_measures = np.array([datoPrueba])
    example_measures = example_measures.reshape(len(example_measures), -1)
    #generamos los sets de entrenami
    clf = GaussianNB()
    clf.fit(X_train, y_train)
    GaussianNB(priors=None)
    prediccion=clf.predict(example_measures)
    # print(clf.predict_proba(X))
    probabilidad=clf.score(X,y)

    cabecera=df.columns
    n2=len(cabecera)
    clases=set(np.array(df['class']))
    return render_template('gauss/result.html',clases=clases,n2=n2,cabecera=cabecera,prediccion=prediccion,ruta=ruta,probabilidad=probabilidad,datoPrueba=datoPrueba,porcentajePrueba=porcentajePrueba)

#cambio para git
if __name__ == '__main__':
   app.run(debug = True)
